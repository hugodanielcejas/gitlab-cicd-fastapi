from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter()


class User(BaseModel):
    id: int
    name: str
    surname: str
    url: str
    age: int


users_list = [User(id=1, name="User1", surname="Name1", url="https://user1.name1", age=35),
              User(id=2, name="User2", surname="Name2",
                   url="https://user2.name2", age=35)]

@router.get("/users")
async def users():
    return users_list