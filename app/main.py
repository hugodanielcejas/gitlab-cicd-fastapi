
# Instalar FastAPI: pip install fastapi
# Instalar uvicorn: pip install uvicorn

from fastapi import FastAPI
from routers import users
app = FastAPI()


@app.get("/")
async def root():
    return "Hola FastAPI!"

# Routers
app.include_router(users.router) 




# Inicia el server: uvicorn main:app --reload
# Detener el server: CTRL+C
# Documentación con Swagger: http://127.0.0.1:8000/docs
# Documentación con Redocly: http://127.0.0.1:8000/redoc